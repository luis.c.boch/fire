extends RigidBody2D

export (PackedScene) var explosion_scene
export var torque = 200.0


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
	
func _process(_delta):
	var bodies = get_colliding_bodies()
	if (bodies.size() > 0):
		generate_explosion(bodies[0])


func _physics_process(delta):
	var direction = Vector2(cos(transform.get_rotation()), sin(transform.get_rotation()))
	add_central_force(delta * direction * torque)

func generate_explosion(_body):
	if (!explosion_scene):
		queue_free()
		return
	var explosion = explosion_scene.instance()
	var debug = Vector2(cos(transform.get_rotation()), sin(transform.get_rotation()))
	
	explosion.global_position = global_position
	explosion.look_at(global_position + -debug)
	get_tree().current_scene.add_child(explosion)
	queue_free()
