extends KinematicBody2D


# export (float) var max_velocity = 1.0
export (float) var acceleration = 500.0

var motion = Vector2()
var control = Vector2()
var direction = Vector2()

onready var left_fire = get_node("Base/Left/FirePosition")
onready var right_fire = get_node("Base/Right/FirePosition")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _physics_process(delta):
	update_control()
	direction = (direction + control).normalized()
	look_at(transform.origin + (direction))
	$Engine.playing = motion.length() > 0.0
	motion = move_and_slide(acceleration * direction)


func update_control(): 
	control = Vector2()
	
	if Input.is_action_pressed("ui_up"):
		control.y = -1
	elif Input.is_action_pressed("ui_down"):
		control.y = 1
	
	if Input.is_action_pressed("ui_left"):
		control.x = -1
	elif Input.is_action_pressed("ui_right"):
		control.x = 1
	
	control = control.normalized()
	
