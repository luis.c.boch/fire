extends Timer

export (PackedScene) var rocket_scene
export var launch_force = 50
export var torque = 100


export (NodePath) var fire_position_path

onready var fire_position: Node2D = get_node(fire_position_path)

var left_rotation: float = deg2rad(135)
var right_rotation: float = deg2rad(-135)

func _on_Fire_timeout():
	if not rocket_scene:
		printerr("No rocket scene defined!")
		return
	
	var rocket: RigidBody2D = rocket_scene.instance()
	
	var transform = Transform2D(
		get_parent().global_rotation,
		fire_position.global_position
	)

	rocket.global_transform = transform

	var rotation = transform.get_rotation()

	rotation += get_rotation_modifier()

	var direction  =   Vector2(cos(rotation), sin(rotation)) 
	
	var force = direction * launch_force

	rocket.apply_central_impulse(force)
	rocket.torque = torque
	
	get_tree().current_scene.add_child(rocket)
	

func get_rotation_modifier() -> float: 
	if randf() > 0.5: 
		return left_rotation  
	else: 
		return right_rotation
