extends Timer

export (PackedScene) var bullet_scene
export var fire_speed = 200.0

export (NodePath) var left_path
export (NodePath) var right_path

onready var left_node: Node2D = get_node(left_path)
onready var right_node: Node2D = get_node(right_path)

var left = false

func get_bullet_scene_transform() -> Vector2: 
	left = !left
	if left:
		return left_node.global_position
	else: 
		return right_node.global_position
		
func _on_Fire_timeout():
	
	if bullet_scene:
		
		var bullet: RigidBody2D = bullet_scene.instance()
		
		var transform = Transform2D(
			get_parent().global_rotation,
			get_bullet_scene_transform()
		)
		
		bullet.global_transform = transform
		
		var force = Vector2(cos(transform.get_rotation()), sin(transform.get_rotation())) * fire_speed
		bullet.apply_central_impulse(force)
		get_tree().current_scene.add_child(bullet)
